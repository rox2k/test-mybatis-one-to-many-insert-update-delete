# test-mybatis-one-to-many-insert

## Configuration

```datasource.properties``` is to define datasource properties.
Each set should have unique prefix.

```ini
# HSQLDB
hsqldb.driver=org.hsqldb.jdbcDriver
hsqldb.url=jdbc:hsqldb:mem:bulkinsert
hsqldb.username=sa
hsqldb.password=

# PostgreSQL
postgresql.driver=org.postgresql.Driver
postgresql.url=jdbc:postgresql:bulkinsert
postgresql.username=bulkinsert
postgresql.password=

# Oracle
oracle.driver=oracle.jdbc.driver.OracleDriver
oracle.url=jdbc:oracle:thin:@192.168.57.101:1521:ORCL
oracle.username=bulkinsert
oracle.password=bulkinsert
```

In ```BulkInsertTestBase.java```, you can configure basic runtime parameters.

```java
// the number of rows to insert
protected static final int numOfRows = 10;
// which datasource setting to use
private static final String propertyPrefix = "mysql";
```



## Ref

- [mybatis-inserts-one-to-many-relationship](https://stackoverflow.com/questions/33028923/mybatis-inserts-one-to-many-relationship) 
- [mybatis-bulk-insert](https://github.com/harawata/mybatis-bulk-insert).

