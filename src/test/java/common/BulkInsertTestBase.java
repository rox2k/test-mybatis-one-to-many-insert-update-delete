/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Note;
import org.apache.ibatis.io.Resources;

import domain.Person;

/**
 * @author Iwao AVE!
 */
public abstract class BulkInsertTestBase {
    protected ObjectMapper jsonMapper = new ObjectMapper();

    protected Person person = createPerson();

    protected static Properties datasourceProperties = loadDataSourceProperties();

    protected static final int numOfRows = 10;

    private static final String propertyPrefix = "postgresql";

    protected static Properties loadDataSourceProperties() {
        Properties result = new Properties();
        try {
            Properties datasourceProperties = Resources.getResourceAsProperties("datasource.properties");
            result.setProperty("driver", datasourceProperties.getProperty(propertyPrefix + ".driver"));
            result.setProperty("url", datasourceProperties.getProperty(propertyPrefix + ".url"));
            result.setProperty("username", datasourceProperties.getProperty(propertyPrefix + ".username"));
            result.setProperty("password", datasourceProperties.getProperty(propertyPrefix + ".password"));
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Failed to load datasource.properties", e);
        }
    }

    protected static Person createPerson() {
        Person person = new Person();
        person.setId("123");
        person.setName("test");

        List<Note> list = new ArrayList<Note>();
        for (int i = 0; i < numOfRows; i++) {
            Note note = new Note();
            note.setId(String.valueOf(i));
            note.setPersonId(person.getId());
            note.setContext("note" + i);
            list.add(note);
        }
        person.setNotes(list);
        return person;
    }

    protected void jsonPrettyPrinter(Object obj) {
        try {
            System.out.println(jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    protected static boolean isOracle() {
        return datasourceProperties.getProperty("url").contains("oracle");
    }
}
